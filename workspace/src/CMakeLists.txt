INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(SERVIDOR_SRCS
	MundoServidor.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)

SET(CLIENTE_SRCS
	MundoCliente.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)

ADD_EXECUTABLE(logger logger.cpp)
ADD_EXECUTABLE(cliente cliente.cpp ${CLIENTE_SRCS})
ADD_EXECUTABLE(servidor servidor.cpp ${SERVIDOR_SRCS})
ADD_EXECUTABLE(bot bot.cpp)

TARGET_LINK_LIBRARIES(cliente glut GL GLU) 		# Igual que tenis en la P3
TARGET_LINK_LIBRARIES(servidor pthread glut GL GLU)	# Igual que tenis en la P3 pero además pthread por hilos
