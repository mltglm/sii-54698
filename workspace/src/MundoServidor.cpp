// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>

//  open y mkfifo
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// read
#include <unistd.h>

// mmap
#include <sys/mman.h>

// señales
#include <signal.h>

#define MAX_PTS 3

void signal_actions(int signal)
{
	printf("\033[33m" "MUNDOSERVIDOR: signal received: " "\033[0m");
	if(signal == SIGUSR2)
	{
		printf("SIGUSR2, ended correctly, returned 0\n\n");
		exit(0);
	}
	if(signal == SIGTERM)
	{
		printf("SIGTERM, kill command used, returned SIGTERM value\n\n");
		exit(SIGTERM);
	}
	if(signal == SIGPIPE)
	{
		printf("SIGPIPE, tried to write without reader, returned SIGPIPE value\n\n");
		exit(SIGPIPE);
	}
	if(signal == SIGINT)
	{
		printf("SIGINT, ctrl+c used, returned SIGINT value\n\n");
		exit(SIGINT);
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	/*close(fd_servidor_cliente);
	unlink(tuberia_servidor_cliente);
	
	close(fd_cliente_servidor);
	unlink(tuberia_cliente_servidor); // Porque manda Servidor, cliente termina al romperse la tubería*/
	
	close(fd_servidor_logger);
	
	printf("MUNDOSERVIDOR: Close() sockets\n");
	//s_conexion.Close();
	//s_comunicacion.Close();
	
	printf("\033[31m" "MUNDOSERVIDOR: destructor\n" "\033[0m");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	// Las 2 próximas líneas son para que las raquetas se paren de mover al levantar la tecla
	//jugador1.velocidad.y=0;
	//jugador2.velocidad.y=0;
	
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		char salida[100];
		sprintf(salida, "El jugador 2 ha marcado un punto, lleva un total de %d punto(s)\n", puntos2);
		write(fd_servidor_logger, salida, strlen(salida));
		
		if(puntos2 >= MAX_PTS)
		{
			//printf("MUNDOSERVIDOR: ejecución exit(1)\n");
			//exit(1);
			kill(getpid(), SIGUSR2);
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		char salida[100];
		sprintf(salida, "El jugador 1 ha marcado un punto, lleva un total de %d punto(s)\n", puntos1);
		write(fd_servidor_logger, salida, strlen(salida));
		if(puntos1 >= MAX_PTS)
		{
			//printf("MUNDOSERVIDOR: ejecución exit(1)\n");
			//exit(1);
			kill(getpid(), SIGUSR2);
		}
	}
	
	s_comunicacion.sock = 5;
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);
	int send_i = s_comunicacion.Send(cad, 200);
	if(send_i < 0) printf("MUNDOSERVIDOR: Error Send() en OnTimer()\n");
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y) {}

void* hilo_comandos(void* d)
{
	//printf("MUNDOSERVIDOR: se ejecuta hilo_comandos()\n");

	CMundo* p=(CMundo*) d;
	p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
     //printf("MUNDOSERVIDOR: se ejecuta RecibeComandosJugador()\n");
     while (1) {
            usleep(10);
            char cad[1];
            int receive_i =  s_comunicacion.Receive(cad, sizeof(cad));
            //if(receive_i < 0) printf("MUNDOSERVIDOR: Error Receive() en RecibeComandosJugador()\n"); Lo quito porque hasta que no se conecta cliente, se imprime muchas veces
            
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='k')jugador2.velocidad.y=-4;
            if(key=='i')jugador2.velocidad.y=4;
      }
      //printf("MUNDOSERVIDOR: fin de RecibeComandosJugador()\n");
}

void CMundo::Init()
{
	umask(0000); // Para deactivar mascara por defecto 0002 y crear los ficheros con los permisos que doy por código

	if(0 != system("pidof -x logger > /dev/null")) // Si comienza a ejecutarse el proceso servidor sin que se esté ejecutando
	{ 					       // antes el proceso logger -> error
		printf("MUNDOSERVIDOR: logger not executing before servidor\n"); 
		exit(1);
	}
	
	///////////////////////////////////////////////////
	// Protección problemas ejecuciones anteriores
	//unlink(tuberia_servidor_cliente);
	//////////////////////////////////////////////////
	
	int i_thread = pthread_create(&thid1, NULL, hilo_comandos, this);
	if(i_thread != 0) printf("MUNDOSERVIDOR: Error creando el hilo\n");
	else printf("MUNDOSERVIDOR: Hilo creado correctamente\n");
        
	fd_servidor_logger=open(tuberia_servidor_logger, O_WRONLY); // Tubería de servidor.cpp a logger.cpp (creada por logger y abierta aquí)
	if(fd_servidor_logger < 0)
	{
                printf("MUNDOSERVIDOR: Error abriendo tuberia_servidor_logger\n");
                exit(1);
        }
	else printf("MUNDOSERVIDOR: tuberia_servidor_logger abierta correctamente\n");	
	
	// Señales
	signal(SIGUSR2, signal_actions);
	
	struct sigaction action1;
	memset(&action1, 0, sizeof(struct sigaction));
	action1.sa_handler = signal_actions;
	sigaction(SIGTERM, &action1, NULL);
	
	struct sigaction action2;
	memset(&action2, 0, sizeof(struct sigaction));
	action2.sa_handler = signal_actions;
	sigaction(SIGPIPE, &action2, NULL);
	
	struct sigaction action3;
	memset(&action3, 0, sizeof(struct sigaction));
	action3.sa_handler = signal_actions;
	sigaction(SIGINT, &action3, NULL);
	
	// Sockets				172.18.5.89
	//int init_i = s_conexion.InitServer("172.18.3.083", 49152); // 127.0.0.001 IP local para comunicación propia máquina // 49152
	
	int init_i = s_conexion.InitServer("192.168.213.046", 49152); // 24 // 49152 funciona Jose 1-213.048
	//int init_i = s_conexion.InitServer("192.168.1.047", 49152); // 24 // 49152 funciona Mia
	
	//int init_i = s_conexion.InitServer("172.018.0.001", 49152);
	//int init_i = s_conexion.InitServer("172.018.001.214", 49152);
	if(init_i < 0) printf("MUNDOSERVIDOR: Error en InitServer()\n");
	else printf("MUNDOSERVIDOR: InitServer() correcto\n");
	
	Socket s_comunicacion = s_conexion.Accept();
	
	/*char buffer[1024] = { 0 };
	int receive_i =  s_comunicacion.Receive(buffer, sizeof(buffer));
	if(receive_i < 0) printf("MUNDOSERVIDOR: Error en Receive()\n");
	else
	{
		printf("\nMUNDOSERVIDOR: Receive() 1 correcto, length = %d\n", receive_i);
		printf("Received: %s\n", buffer);
	}
	
	int send_i = s_comunicacion.Send("Servidor -> Cliente\n", 1024);
	if(send_i < 0) printf("MUNDOSERVIDOR: Error en Send()\n");
	else printf("MUNDOSERVIDOR: Send() 2 correcto, length = %d\n\n", send_i);*/
	

	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
