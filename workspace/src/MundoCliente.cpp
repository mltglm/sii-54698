// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//  open y mkfifo
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// read
#include <unistd.h>

// mmap
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	// munmap(puntdatos, s.st_size);
	unlink(fichero);

	printf("\033[31m" "MUNDOCLIENTE: destructor\n" "\033[0m");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{				
	char cad[200];
	int receive_i =  s_comunicacion.Receive(cad, sizeof(cad));
	if(receive_i < 0) ;//printf("MUNDOCLIENTE: Error Receive() en OnTimer()\n");
        else
        {
		sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);
		
		puntdatos->esfera=esfera;
		puntdatos->raqueta1=jugador1;
		
		//if(puntdatos->accion==-1) OnKeyboardDown('s', 0, 0);
		//if(puntdatos->accion==1) OnKeyboardDown('w', 0, 0);
	
		switch(puntdatos->accion) // Comprobar teclas bien con cada accion
		{
			case -1	:
				CMundo::OnKeyboardDown((unsigned char)'s',0,0);
				break;
			case 0	:
				CMundo::OnKeyboardDown((unsigned char)'a',0,0);
				break;
			case 1	:
				CMundo::OnKeyboardDown((unsigned char)'w',0,0);
				break;
			default: break;
		}
	}
	
	// MUY LENTO
	/*if(0 != system("pidof -x servidor > /dev/null")) // Si comienza a ejecutarse el proceso cliente sin que se esté ejecutando
	{ 					       // antes el proceso logger -> error
		printf("\033[33m" "MUNDOCLIENTE: Ha terminado servidor, por lo que termina cliente\n" "\033[0m");
		exit(1);
	}*/
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char key_new = (char)key;
	// s_comunicacion.sock = 3; Ahora no hace falta
	int send_i = s_comunicacion.Send(&key_new, 1);
	if(send_i < 0) printf("MUNDOCLIENTE: Error Send() en OnKeyboardDown()\n");
}

void CMundo::Init()
{
	umask(0000); // Para deactivar mascara por defecto 0002 y crear los ficheros con los permisos que doy por código
	
	/*if(0 != system("pidof -x logger > /dev/null")) // Si comienza a ejecutarse el proceso cliente sin que se esté ejecutando
	{ 					       // antes el proceso logger -> error
		printf("MUNDOCLIENTE: logger not executing before MundoCliente\n"); 
		exit(1);
	}
	
	if(0 != system("pidof -x servidor > /dev/null")) // Si comienza a ejecutarse el proceso cliente sin que se esté ejecutando
	{ 					       // antes el proceso servidor -> error
		printf("MUNDOCLIENTE: servidor not executing before MundoCliente\n"); 
		exit(1);
	}*/
	
	///////////////////////////////////////////////////
	// Protección problemas ejecuciones anteriores
	unlink(fichero); // Fichero proyectado en memoria, también se puede poner al final del código del bot, después de munmap(punt_dat, s.st_size);
	///////////////////////////////////////////////////
	
	puntdatos=&datos;
	
	// IMPORTANTE: no puedo hacer mmap con fichero creado con mkfifo
	
	/*int i_mkfifo = mkfifo(fichero, 0777); // tenis es el encargado de crear el fichero a proyectar
	if (i_mkfifo < 0)
	{
		printf("TENIS: Error creando el fichero\n");
		perror("mkfifo() error");
		exit(1);
	}
	else printf("TENIS: fichero creado correctamente\n");*/
	
	f_datosmem=open(fichero, O_RDWR|O_CREAT|O_TRUNC, 0777);
		
	if(f_datosmem < 0)
	{
		printf("MUNDOCLIENTE: Error creando o abriendo el fichero\n");
		exit(1);
	}
	else
	{
		printf("MUNDOCLIENTE: Fichero creado y abierto correctamente\n");
		write(f_datosmem, &datos, sizeof(DatosMemCompartida));
		struct stat s;
		fstat(f_datosmem, &s);
		puntdatos=(DatosMemCompartida *)mmap(NULL, s.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, f_datosmem, 0);
		if(puntdatos == MAP_FAILED)
        	{
			printf("MUNDOCLIENTE: Error proyectando el fichero\n");
			perror("mmap() error");
		}
		else printf("MUNDOCLIENTE: Fichero proyectado correctamente\n");
		close(f_datosmem);
		
		// Sockets
		
		int connect_i = s_comunicacion.Connect("192.168.213.046", 49152); // "127.0.0.001", 49152 Jose ("192.168.1.048", 49152)
		//int connect_i = s_comunicacion.InitServer("192.168.1.047", 49152); // 24 // 49152 funciona Mia
		
		if(connect_i < 0) printf("MUNDOCLIENTE: Error en Connect()\n");
		else printf("MUNDOCLIENTE: Connect() correcto\n");
		
		/*int send_i = s_comunicacion.Send("Cliente -> Servidor\n", 1024);
		if(send_i < 0) printf("MUNDOCLIENTE: Error en Send()\n");
		else printf("\nMUNDOCLIENTE: Send() 1 correcto, length = %d\n\n", send_i);
		
		char buffer[1024] = { 0 };
		int receive_i =  s_comunicacion.Receive(buffer, sizeof(buffer));
		if(receive_i < 0) printf("MUNDOCLIENTE: Error en Receive()\n");
		else
		{
			printf("MUNDOCLIENTE: Receive() 2 correcto, length = %d\n", receive_i);
			printf("Received: %s\n", buffer);
		}*/
		
		Plano p;
		//pared inferior
		p.x1=-7;p.y1=-5;
		p.x2=7;p.y2=-5;
		paredes.push_back(p);

		//superior
		p.x1=-7;p.y1=5;
		p.x2=7;p.y2=5;
		paredes.push_back(p);
	
		fondo_izq.r=0;
		fondo_izq.x1=-7;fondo_izq.y1=-5;
		fondo_izq.x2=-7;fondo_izq.y2=5;
	
		fondo_dcho.r=0;
		fondo_dcho.x1=7;fondo_dcho.y1=-5;
		fondo_dcho.x2=7;fondo_dcho.y2=5;

		//a la izq
		jugador1.g=0;
		jugador1.x1=-6;jugador1.y1=-1;
		jugador1.x2=-6;jugador1.y2=1;

		//a la dcha
		jugador2.g=0;
		jugador2.x1=6;jugador2.y1=-1;
		jugador2.x2=6;jugador2.y2=1;
	}	
}
