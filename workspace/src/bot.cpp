#include <stdlib.h>
#include <stdio.h>

//  open, stat, fstat
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// read, close usleep
#include <unistd.h>

// mmap
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(int argc, char *argv[]){
	int fd_fichero;
	char *fichero= "/tmp/fichero";
	DatosMemCompartida *punt_dat;
	
	umask(0000); // Para deactivar mascara por defecto 0002 y crear los ficheros con los permisos que doy por código
	
	if(0 != system("pidof -x cliente > /dev/null")){ 	          // Si comienza a ejecutarse el proceso bot sin que se esté ejecutando
		printf("BOT: cliente not executing before bot\n");      // antes el proceso cliente -> error
		return 1;
	}
	
	fd_fichero = open(fichero, O_RDWR);
	while(fd_fichero < 0) fd_fichero = open(fichero, O_RDWR);
	if(fd_fichero < 0){
		printf("BOT: Error abriendo el fichero\n");
		unlink(fichero);
		return 1;
	}
	else
	{
		printf("BOT: Fichero abierto correctamente\n");
		struct stat s;
		
		int i;
		while(i<200)
		{
			munmap(punt_dat, s.st_size);
			i++;
		}
		
		fstat(fd_fichero, &s);
		sleep(1); // 1 segundo
		punt_dat = (DatosMemCompartida *)mmap(NULL, s.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, fd_fichero, 0);
	        if(punt_dat == MAP_FAILED)
	        {
			printf("BOT: Error proyectando el fichero\n");
			perror("mmap() error");
			unlink(fichero);
			return 1;
		}
		else printf("BOT: Fichero proyectado correctamente\n\n");
	        close(fd_fichero);

		while(1)
		{
			usleep(25000); // Espera 25 ms
			
			// Lógica movimiento
			if(punt_dat->esfera.centro.y > (punt_dat->raqueta1.y1+punt_dat->raqueta1.y2)/2.0) // Esfera por encima del jugador
				punt_dat->accion=1; // Subir
			else if(punt_dat->esfera.centro.y < (punt_dat->raqueta1.y1+punt_dat->raqueta1.y2)/2.0) // Esfera por debajo del jugador
      	                	punt_dat->accion=-1; // Bajar
			else punt_dat->accion=0; // Nada
				
			// Finalización bucle
			if(0 != system("pidof -x cliente > /dev/null"))
			{
				printf("\033[33m" "BOT: Ha terminado cliente, por lo que termina bot\n" "\033[0m");
				break; // Si termina de ejecutarse proceso cliente, termina bot
			}
		}
		munmap(punt_dat, s.st_size);
		unlink(fichero);
		return 0;
	}
}
