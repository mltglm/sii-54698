// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

// sockets
#include "Socket.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	// Comunicación entre Cliente y Servidor (ambos sentidos)
	/*int fd_servidor_cliente;
	char *tuberia_servidor_cliente= "/tmp/tuberia_servidor_cliente";
	
	int fd_cliente_servidor;
        char *tuberia_cliente_servidor= "/tmp/tuberia_cliente_servidor";*/
        
        pthread_t thid1;
        void RecibeComandosJugador();
	
	// Logger
	int fd_servidor_logger;
	char *tuberia_servidor_logger= "/tmp/tuberia_servidor_logger";
	
	// Sockets
	Socket s_conexion;
	Socket s_comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
